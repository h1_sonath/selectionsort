﻿using System;

namespace Array
{
    class Program
    {
		static void Main(string[] args)
		{
			int[] a = new int[5] { 5, 1, 2, 4, 3 };
			int n = 5;
			for (int i = 0; i < n - 1; i++)
			{
				int minIndex = i;
				for (int j = i + 1; j < n; j++)
				{
					if (a[minIndex] > a[j])
						minIndex = j;
				}
				if (i != minIndex)
				{
					int temp = a[i];
					a[i] = a[minIndex];
					a[minIndex] = temp;
				}
			}
			Console.Write("Sorted array is: ");
			for (int i = 0; i < n; i++)
			{
				Console.Write(a[i] + " ");
			}
		}
	}
}
